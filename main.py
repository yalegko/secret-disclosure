import goless

from protocol import Protocol
from protocol.roles import Owner, Buyer

Protocol.new(
    secret_bits=12,
    nsecrets=5,
    key_bits=17
)
goless.on_panic = Protocol.finish

Protocol.logdebug("Far far away from us...")
alice = Owner(name="Alice")
alice.gen_secrets()

bob = Buyer(name="Bob", desired_secret=3)
carol = Buyer(name="Carol", desired_secret=1)


Protocol.logdebug("Now all that great people should meet themselves")
alice.add_buyer(bob)
alice.add_buyer(carol)
bob.add_rival(carol)
carol.add_rival(bob)

Protocol.logdebug("%r wanted to buy %d" % (bob, alice.secrets[bob.desired_secret]))
Protocol.logdebug("%r wanted to buy %d" % (carol, alice.secrets[carol.desired_secret]))

Protocol.logdebug("Let the PROTOCOL begin!")
goless.go(alice.serve)
goless.go(bob.serve)
goless.go(carol.serve)

try:
    Protocol.wait_for_finish()
except goless.backends.Deadlock:
    print "WoW, deadlock is bad, mkeey?!"

