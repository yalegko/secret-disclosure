# All-or-Nothing Disclosure of Secrets
> **Applied Cryptography, Second Edition: Protocols, Algorithms, and Source Code in C** 
by Bruce Schneier

## Intro
That repository contains implementation of the "All-or-Nothing Disclosure of Secrets"
protocol.

Usage example see in [main.py](./main.py)

## Abstract
Imagine that Alice is a former agent of the former Soviet Union, now
unemployed. In order to make money, Alice sells secrets. Anyone who is
willing to pay the price can buy a secret. She even has a catalog. All her
secrets are listed by number, with tantalizing titles: “Where is Jimmy Hoffa?”,
“Who is secretly controlling the Trilateral Commission?”, “Why does Boris
Yeltsin always look like he swallowed a live frog?”, and so on.

Alice won’t give away two secrets for the price of one or even partial
information about any of the secrets. Bob, a potential buyer, doesn’t want to
pay for random secrets. He also doesn’t want to tell Alice which secrets he
wants. It’s none of Alice’s business, and besides, Alice could then add “what
secrets Bob is interested in” to her catalog.

A poker protocol won’t work in this case, because at the end of the protocol
Alice and Bob have to reveal their hands to each other. There are also tricks
Bob can do to learn more than one secret.

The solution is called all-or-nothing disclosure of secrets (ANDOS)
because, as soon as Bob has gained any information whatsoever about one of
Alice’s secrets, he has wasted his chance to learn anything about any of the
other secrets.

## Formal
Formal description is presented in [here(html)](docs/disclosure.html) 
and [here(pdf)](docs/disclosure.pdf)