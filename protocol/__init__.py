import goless
import logging
from Crypto import Random
rnd = Random.new()


class Message:
    def __init__(self, cmd_type, args):
        self.type = cmd_type
        self.args = args


class Protocol:
    # default
    nsecrets = 8
    secret_bits = 10
    secret_mask = (1 << secret_bits) - 1
    key_bits = 17

    logger = logging.getLogger("Protocol")
    logger.setLevel(logging.DEBUG)
    loggerHndl = logging.StreamHandler()
    loggerHndl.setFormatter(logging.Formatter('%(asctime)s - %(person_name)10s : %(message)s'))
    logger.addHandler(loggerHndl)

    finish_chan = goless.chan()

    @classmethod
    def new(cls, secret_bits, key_bits, nsecrets):
        cls.secret_bits = secret_bits
        cls.secret_mask = (1 << secret_bits) - 1
        cls.key_bits = key_bits
        cls.nsecrets = nsecrets

    @classmethod
    def set_logger(cls, logger):
        cls.logger = logger

    @classmethod
    def gen_numbers(cls):
        return [
            random_bits(cls.secret_bits)
            for _ in xrange(cls.nsecrets)
        ]

    @classmethod
    def logwarn(cls, msg):
        cls.logger.warn(msg,
                        extra=dict(person_name="SUPERVISOR"))

    @classmethod
    def logdebug(cls, msg):
        cls.logger.debug(msg,
                         extra=dict(person_name="SUPERVISOR"))

    @classmethod
    def finish(cls, etype=None, value=None, tb=None):
        if value is not None:
            cls.logwarn('Got an exception %s!' % value)
        else:
            cls.logdebug('PROTOCOL finished!')
        cls.finish_chan.send(True)

    @classmethod
    def wait_for_finish(cls):
        cls.finish_chan.recv()


class ProtocolError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class FBI:
    def __init__(self, a, b):
        self.value = a ^ b ^ Protocol.secret_mask

    def complement_to(self, *numbers):
        return [
            self.value ^ n ^ Protocol.secret_mask
            for n in numbers
        ]

    def __repr__(self):
        i, res, fbi = 0, [], self.value
        while fbi != 0 and i < Protocol.secret_bits:
            if fbi & 1:
                res.append(i)
            fbi >>= 1
            i += 1
        return res.__repr__()


def send(recipient, message):
    recipient.chan.send(message)


def random_bits(nbits):
    nbytes = nbits / 8
    # bits in most significant byte
    msbits = nbits % 8

    res = ord(rnd.read(1))
    res &= (1 << msbits) - 1
    res |= 1 << (msbits - 1)
    for i in xrange(nbytes):
        res = res * 256 + ord(rnd.read(1))

    assert len(bin(res)[2:]) == Protocol.secret_bits
    return res


# set returns an integer with the bit at 'offset' set to 1.
def toggle_bit(int_type, offset):
    mask = 1 << offset
    return int_type ^ mask