import rsa


class Key:
    def __init__(self, nbits):
        self.public, self.private = rsa.newkeys(nbits)


def encrypt_int(n, key):
    return pow(n, key.e, key.n)


def decrypt_int(cn, key):
    return pow(cn, key.d, key.n)
