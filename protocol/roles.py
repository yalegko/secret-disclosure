import minirsa as rsa
from protocol import *


class Person(object):
    def __init__(self, name):
        self.name = name
        self.log("Hey, I'm born!")

    def log(self, msg):
        Protocol.logger.debug(msg, extra=dict(person_name=self.name))

    def __repr__(self):
        return "<Person %s>" % self.name


class Owner(Person):
    def __init__(self, name):
        super(Owner, self).__init__(name)
        self.chan = goless.chan(1)

        self.secrets = []
        self.buyers = []
        self.keys = {}

    def add_buyer(self, buyer):
        self.buyers.append(buyer)
        buyer.vendor = self
        self.log("Just meet a buyer %r" % buyer)

    def gen_secrets(self):
        self.secrets = Protocol.gen_numbers()
        self.log("Yay, I have the secrets %r" % self.secrets)

    def gen_keys(self, recipient):
        new_key = rsa.Key(Protocol.key_bits)
        self.keys[recipient] = new_key.private
        return new_key.public

    def buyers_except(self, buyer):
        for b in self.buyers:
            if b != buyer:
                yield b

    def serve(self):
        if len(self.secrets) == 0:
            raise ProtocolError("Please generate secrets for owner")
        if len(self.buyers) == 0:
            raise ProtocolError("No buyers found!")

        for buyer in self.buyers:
            cmd = Message("keys", (self.gen_keys(buyer)))
            send(buyer, cmd)

        # (5)
        for _ in self.buyers:
            # Get complemented numbers from buyers
            msg = self.chan.recv()
            if msg.type != "complement":
                raise ProtocolError("I get %s instead complemented numbers!" % msg.type)
            numbers, sender = msg.args

            # Now send decrypted numbers to all buyers except sender
            for b in self.buyers_except(sender):
                numbers = [
                    rsa.decrypt_int(x, self.keys[b]) ^ y
                    for x, y in zip(numbers, self.secrets)
                ]
                self.log('Sending final numbers to %r' % b)
                msg = Message("final", (numbers,))
                send(b, msg)

        Protocol.finish()


class Buyer(Person):
    def __init__(self, name, desired_secret):
        super(Buyer, self).__init__(name)
        if desired_secret < 0 or desired_secret >= Protocol.nsecrets:
            raise ProtocolError("No such secret in current Protocol configuration")

        self.desired_secret = desired_secret
        self.chan = goless.chan(1)
        self.log("Hmm, #%d secret seems really cool. I'm going to buy it" % self.desired_secret)

        # Following will be used as public properties, so I'll mention it here
        self.fake_secrets = []
        self.rival = None
        self.key = None
        self.vendor = None
        self.secret = None

    def add_rival(self, rival):
        self.rival = rival
        self.log("Meet a strange looking %r. Seems that he's my rival" % rival)

    def gen_fake_secrets(self):
        self.fake_secrets = Protocol.gen_numbers()
        self.log("Huh, just generate a bunch of fake numbers %r" % self.fake_secrets)
        return self.fake_secrets

    def serve(self):
        # Wait for keys from secrets' owner
        msg = self.chan.recv()
        if msg.type != "keys":
            raise ProtocolError("Got not a keys as the first message")
        self.key = msg.args
        self.log("Got public key %r" % (self.key,))

        # Gen some numbers look exactly like secrets
        self.gen_fake_secrets()

        # Send it to the rival and got his ones
        msg = Message("numbers", (self.fake_secrets,))
        send(self.rival, msg)

        msg = self.chan.recv()
        if msg.type != "numbers":
            raise ProtocolError("Didn't get fake secrets from rival")
        rival_numbers = msg.args[0]
        self.log("Got some numbers from my rival: %r" % (rival_numbers,))

        # Now I should encrypt C_b where b is a number of desired secret
        rival_secret = rival_numbers[self.desired_secret]
        enc_secret = rsa.encrypt_int(
            rival_secret,
            self.key
        )

        # Calc FBI of the received and encrypted one and send it to rival
        fbi = FBI(enc_secret, rival_secret)
        self.log("Encrypted rival's %d into %d and got FBI %r" % (
            rival_secret, enc_secret, fbi
        ))
        msg = Message("fbi", (fbi,))
        send(self.rival, msg)

        # Wait for the rival's FBI
        msg = self.chan.recv()
        if msg.type != "fbi":
            raise ProtocolError("Didn't get FBI from my rival!")
        rival_fbi = msg.args[0]
        self.log("Got FBI from my rival %r" % rival_fbi)

        # (4)
        # Now I should calc complement for my number and send it to vendor
        complement = rival_fbi.complement_to(*self.fake_secrets)
        self.log("Going to send complemented numbers to vendor %r" % complement)

        msg = Message("complement", (complement, self))
        send(self.vendor, msg)

        # (5)
        # Wait for numbers from vendor
        msg = self.chan.recv()
        if msg.type != "final":
            raise ProtocolError("Got %s instead final numbers" % msg.type)
        final_numbers = msg.args[0]

        self.secret = final_numbers[self.desired_secret] ^ rival_numbers[self.desired_secret]
        self.log("Yay!! I got desired secret: %d!!1" % self.secret)


